/**
 * @file
 * Initialize the Reveal handler.
 */

(function () {
	// Use a random theme if reveal container has 'pattern--random' class.
	const themes = [
		'hills',
		'indigo',
		'molecules',
		'night-sky',
		'orange-river',
		'palette',
		'playground',
		'pop',
		'quilt',
		'stream',
		'stripes',
		'waterfall',
		'waves',
	];
	document.addEventListener('DOMContentLoaded', function () {
		const reveal = document.getElementsByClassName('reveal').item(0);
		if (!reveal.classList.contains('pattern--random')) {
			return;
		}

		reveal.classList.forEach(function (value) {
			if (value.substr(0, 7) === 'pattern') {
				reveal.classList.remove(value);
			}
		});
		const index = Math.floor(Math.random() * themes.length);
		reveal.classList.add('pattern--' + themes[index]);
	});


	// More info about config & dependencies:
	// - https://github.com/hakimel/reveal.js#configuration
	// - https://github.com/hakimel/reveal.js#dependencies
	Reveal.initialize({
			center: false,
			width: "100%",
			height: "100%",
			margin: 0,
			minScale: 1,
			maxScale: 1,
			history: true,

			// Arrow keys progress through sub-slides
			// keyboard: {
			// 	39: 'next', // right key
			// 	37: 'prev'	// left key
			// },

   // Math configuration
   math: {
					// mathjax: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js',
					config: 'TeX-AMS_HTML-full',
					TeX: {
						Macros: {
							R: '\\mathbb{R}',
							set: [ '\\left\\{#1 \\; ; \\; #2\\right\\}', 2 ]
						}
					}
			},

   // Table of contents
   tableofcontents: {
					// Specifies the slide title of the table of contents slide
					title: "Table of Contents",

					// Specifies the position of the table of contents slide in the presentation
					position: 2,

					// Specifies which slide tag elements will be used
					// for generating the table of contents.
					titleTagSelector: "h1, h2, h3, h4, h5, h6",

					// Specifies if the first slide, mostly the title slide of the presentation, should be ignored.
					ignoreFirstSlide: false,

					// Specifies if every single element of the table of contents
					// will be stepped through before moving on to the next slide.
					fadeInElements: false
			},


			dependencies: [
					{ src: 'plugin/markdown/marked.js' },
					{ src: 'plugin/markdown/markdown.js' },
					{ src: 'plugin/zoom-js/zoom.js', async: true },
					{ src: 'plugin/notes/notes.js', async: true },
					{ src: 'plugin/math/math.js', async: true },
     //{ src: 'plugin/tableofcontents/tableofcontents.js' },
					{ src: 'plugin/highlight/highlight.js',
							async: true,
							condition: function () {
									// Trim whitespace from code blocks, and prevent HTML escaping by default.
									// This must be done before the plugin is loaded.
									Array.prototype.forEach.call(
											document.querySelectorAll('pre code'),
											function (element) {
													if (!element.hasAttribute('data-notrim')) {
															element.setAttribute('data-trim', '');
													}
													if (!element.hasAttribute('data-escape')) {
															element.setAttribute('data-noescape', '');
													}
											}
									);
									return true;
							},
							callback: function () {
									hljs.initHighlightingOnLoad();
							}
					}
			]
	});

}());
